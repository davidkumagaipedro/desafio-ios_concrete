//
//  GithubConcreteTests.swift
//  GithubConcreteTests
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import XCTest

@testable import GithubConcrete

class GithubConcreteTests: XCTestCase {
    
    var pullRequestViewController: PullRequestViewController!
    var userJavaViewController: UserJavaViewController!
    
    override func setUp() {
        super.setUp()
        
        userJavaViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserJavaViewController") as! UserJavaViewController
        _ = userJavaViewController.view
        
        pullRequestViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PullRequestViewController") as! PullRequestViewController
        _ = pullRequestViewController.view
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewDidLoadPullRequestViewController(){
        pullRequestViewController.viewDidLoad()
    }
    
    func testViewDidLoadUserJavaViewController(){
        userJavaViewController.viewDidLoad()
    }
    
    func testTableViewRowHeightOfPullRequestViewController(){
        XCTAssertEqual(pullRequestViewController.tableView.rowHeight, -1.0)
    }
    
    func testTableViewRowHeightOfUserJavaViewController(){
        XCTAssertEqual(userJavaViewController.tableView.rowHeight, -1.0)
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
