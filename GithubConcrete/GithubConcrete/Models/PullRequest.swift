//
//  PullRequestModel.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest {
    
    var userPullRequest: UsuarioPullRequest?
    var infoPullRequest: InfoPullRequest?
    var repository: Repositorio?
    
    init(user: UsuarioPullRequest, info: InfoPullRequest, repo: Repositorio){
        userPullRequest = user
        infoPullRequest = info
        repository = repo
    }
}






