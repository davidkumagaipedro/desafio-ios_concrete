//
//  Owner.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

struct Owner: Mappable {
    
    var name: String?
    var avatar_url: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map){
        name <- map["login"]
        avatar_url <- map["avatar_url"]
    }
    
    init(nick: String, url: String) {
        name = nick
        avatar_url = url
    }
}
