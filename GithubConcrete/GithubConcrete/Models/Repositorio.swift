//
//  Repository.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

struct Repositorio: Mappable {
    
    var nomeRepositorio: String?
    
    init?(map: Map) {}
    mutating func mapping(map: Map) {
        nomeRepositorio <- map["full_name"]
    }
    
    init(name: String){
        nomeRepositorio = name
    }
}
