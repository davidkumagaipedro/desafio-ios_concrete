//
//  User.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

class Usuario {
    
    var usuarioGit : UsuarioGithub?
    var ownerRepo: Owner?
    
    init(user: UsuarioGithub, owner: Owner){
        usuarioGit = user
        ownerRepo = owner
    }
    
}
