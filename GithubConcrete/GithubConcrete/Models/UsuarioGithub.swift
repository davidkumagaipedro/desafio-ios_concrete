//
//  UsuarioGitHub.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

struct UsuarioGithub: Mappable {
    
    var repositorioNome: String?
    var full_name: String?
    var description_repository: String?
    var stars: Int?
    var forks: Int?
    
    init?(map: Map){
    }
    
    mutating func mapping(map: Map) {
        repositorioNome <- map["name"]
        description_repository <- map["description"]
        stars <- map["stargazers_count"]
        forks <- map["forks_count"]
        full_name <- map["full_name"]
    }
    
    init(name: String, fullname: String, desc: String, star: Int, fork: Int) {
        repositorioNome = name
        full_name = fullname
        description_repository = desc
        stars = star
        forks = fork
    }
}



