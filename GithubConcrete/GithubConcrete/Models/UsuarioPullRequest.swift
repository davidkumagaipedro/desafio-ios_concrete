//
//  UsuarioPullRequest.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

struct UsuarioPullRequest: Mappable {
    
    var name_user: String?
    var image_user: String?
    
    init?(map: Map){
    }
    
    mutating func mapping(map: Map) {
        name_user <- map["login"]
        image_user <- map["avatar_url"]
    }
    
    init(name: String, image: String) {
        name_user = name
        image_user = image
    }
}
