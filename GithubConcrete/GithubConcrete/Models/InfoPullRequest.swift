//
//  InfoPullRequest.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import ObjectMapper

struct InfoPullRequest: Mappable {
    
    var tituloPullRequest: String?
    var bodyPullRequest: String?
    var pageWebUrl: String?
    
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map){
        tituloPullRequest <- map["title"]
        bodyPullRequest <- map["body"]
        pageWebUrl <- map["html_url"]
    }
    
    init(title: String, body: String, url: String) {
        tituloPullRequest = title
        bodyPullRequest = body
        pageWebUrl = url
    }
}
