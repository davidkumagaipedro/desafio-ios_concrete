//
//  UserTableViewDataSource.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

class UserTableViewDataSource: NSObject, UITableViewDataSource {
    
    var items: [Usuario] = []
    var tableView: UITableView?
    var delegate: UITableViewDelegate?
    
    required init(items: [Usuario], tableview: UITableView, delegate: UITableViewDelegate) {
        self.items = items
        self.tableView = tableview
        self.delegate = delegate
            
        super.init()
    }
    
    func UpdateTable(newItems: [Usuario]){
        for newItem in newItems {
            self.items.append(newItem)
        }
        self.tableView?.reloadData()
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
            
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserTableViewCell
        cell.setup(user: items[indexPath.row])
        
        return cell
    }
    
    func getNameOwnerOfRepo(index: Int) -> String? {
        return items[index].ownerRepo?.name
    }
    
    func getNameRepo(index: Int) -> String? {
        return items[index].usuarioGit?.repositorioNome
    }
    
    
}
