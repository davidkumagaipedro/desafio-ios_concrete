//
//  PullRequestTableViewDataSource.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

//DataSource do PullRequestViewController
class PullRequestTableViewDataSource: NSObject, UITableViewDataSource{
    
    var items: [PullRequest] = []
    var tableView: UITableView?
    var delegate: UITableViewDelegate?
    
    //Instancia
    required init(items: [PullRequest], tableview: UITableView, delegate: UITableViewDelegate) {
        self.items = items
        self.tableView = tableview
        self.delegate = delegate
        
        super.init()
    }
    
    //Retorno da Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
        
    }
    
    //Setup da CEl
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! PullRequestTableViewCell
        cell.setup(pullrequest: items[indexPath.row])
        
        return cell
    }
}
