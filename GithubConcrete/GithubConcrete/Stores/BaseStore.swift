//
//  BaseStore.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class BaseStore: NSObject {
    
    static var controlRequest : Charge = .incosistente
    
    //Função que quebra o JSON
    static func loadRep(page: Int, complete : @escaping ([Usuario]) -> Void) {
        
        if BaseStore.controlRequest == .incosistente{
            Alamofire.request(WebServices.repositories.setPage(page: page)).responseJSON {
                response in switch response.result {
                    
                case .failure(let error):
                    print("Erro no request: \(error)")
                    
                case .success(let data):
                    guard let jsonString = convertToJSONString(from: data) else {
                        return
                        
                    }
                    guard let dict = convertToDictionary(text: jsonString) else {
                        return
                        
                    }
                    guard let items = dict["items"] as? [[String: Any]] else {
                        return
                        
                    }
                    
                    guard let statusControlRequest = dict["incomplete_results"] as? Int else {
                        return
                        
                    }
                    
                    
                    self.setRequest(status: statusControlRequest)
                    
                    var users : [Usuario] = []
                    for item in items {
                        guard let ownerString = convertToJSONString(from: item["owner"] ?? "") else { return
                            
                        }
                        guard let ownerRepo = Mapper<Owner>().map(JSONString: ownerString) else {
                            return
                            
                        }
                        guard let userGitHub = Mapper<UsuarioGithub>().map(JSON: item) else {
                            return
                            
                        }
                        
                        let user = Usuario(user: userGitHub, owner: ownerRepo)
                        users.append(user)
                    }
                    
                    complete(users)
                    
                
                }
            }
        } else {
            print("Tudo foi Baixado")
        }
    }
    
    //Uso uma função para quebrar o JSON
    static func loadPull(username: String, name_repo: String, complete: @escaping ([PullRequest]) -> Void) {
        
        let url = "https://api.github.com/repos/\(username)/\(name_repo)/pulls"
        
        Alamofire.request(url).responseJSON {
            response in switch response.result {
                
            case .success(let data):
                guard let jsonString = convertToJSONString(from: data) else {
                    return
                    
                }
                guard let arrayJson = convertToArray(text: jsonString) else {
                    return
                    
                }
                
                var pullrequests : [PullRequest] = []
                for item in arrayJson {
                    guard let jsonString = convertToJSONString(from: item) else {
                        return
                        
                    }
                    guard let i_PullRequest = Mapper<InfoPullRequest>().map(JSONString: jsonString) else {
                        return
                        
                    }
                    
                    guard let dict = convertToDictionary(text: jsonString) else {
                        return
                        
                    }
                    guard let js_User = convertToJSONString(from: dict["user"] ?? "") else {
                        return
                        
                    }
                    guard let user_PullRequest = Mapper<UsuarioPullRequest>().map(JSONString: js_User) else {
                        return
                        
                    }
                    
                    guard let js_String = convertToJSONString(from: dict["head"] ?? "") else {
                        return
                        
                    }
                    guard let js_String_rep = convertToDictionary(text: js_String) else {
                        return
                        
                    }
                    
                    var repo: Repositorio
                    
                    if js_String_rep["repo"] is NSNull {
                        repo = Repositorio(name: "Repository Null")
                    } else {
                        guard let JSONRep = convertToJSONString(from: js_String_rep["repo"] ?? "") else {
                            return
                            
                        }
                        guard let repository = Mapper<Repositorio>().map(JSONString: JSONRep) else {
                            return
                            
                        }
                        
                        repo = repository
                    }
                    
                    let pullrequest = PullRequest(user: user_PullRequest, info: i_PullRequest, repo: repo)
                    pullrequests.append(pullrequest)
                }
                
                complete(pullrequests)
                
            case .failure(let error):
                print("Request falha com erro: \(error)")
            }
        }
    }
    
    static func setRequest(status: Int){
        switch status {
        case 0:
            BaseStore.controlRequest = .incosistente
        case 1:
            BaseStore.controlRequest = .consistente
        default:
            break
        }
    }
}
