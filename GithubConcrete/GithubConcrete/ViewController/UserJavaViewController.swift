//
//  ViewController.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

class UserJavaViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var page = 1
    var dataSource: UserTableViewDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        self.setupTableView()
        tableView.delegate = self
        self.navigationItem.title = "GitHub Java"
    }

    func setupTableView(){
        BaseStore.loadRep(page: self.page) { (users) in
            self.dataSource = UserTableViewDataSource(items: users, tableview: self.tableView, delegate: self)
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
            self.tableView.isHidden = false
            }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let countItems = self.dataSource?.items.count else { return }
        if indexPath.row == countItems - 1 {
            self.page += 1
            BaseStore.loadRep(page: self.page, complete: { (users) in
                self.dataSource?.UpdateTable(newItems: users)
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PullRequestViewController") as? PullRequestViewController
        
        viewController?.name_repo = dataSource?.getNameRepo(index: indexPath.row)
        viewController?.user_owner = dataSource?.getNameOwnerOfRepo(index: indexPath.row)
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

