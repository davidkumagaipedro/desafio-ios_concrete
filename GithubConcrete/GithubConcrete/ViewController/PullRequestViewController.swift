//
//  SecondViewController.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

class PullRequestViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var user_owner : String?
    var name_repo : String?
    var dataSource : PullRequestTableViewDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isHidden = true
        self.setupTableView()
        tableView.delegate = self
        self.navigationItem.title = "Pull Request"
    }
    
    func setupTableView(){
        if user_owner != "" && name_repo != "" {
            BaseStore.loadPull(username: user_owner ?? "", name_repo: name_repo ?? "") { (pullrequests) in
                self.dataSource = PullRequestTableViewDataSource(items: pullrequests, tableview: self.tableView, delegate: self)
                self.tableView.dataSource = self.dataSource
                self.tableView.reloadData()
                self.tableView.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let select = dataSource?.items[indexPath.row].infoPullRequest?.pageWebUrl else {
            return
            
        }
        loading(load: select)
    }
    
    func loading(load: String){
        guard let url = URL(string: load) else { return }
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

}
