//
//  Charge.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

//Enum para controle de carga
enum Charge {
    case incosistente
    case consistente
}
