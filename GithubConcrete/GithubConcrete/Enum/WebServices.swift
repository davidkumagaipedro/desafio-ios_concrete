//
//  WebServices.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

//enum de retorno no WebService
enum WebServices : String {
    case repositories
    
    //Pagina de retorno
    func setPage(page: Int) -> String{
        switch self {
        case .repositories:
            return "https://api.github.com/search/repositories?q=language:Java&sort=start&page=\(page)" as String
        }
    }
}
