//
//  UIExtension.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func roundBorder() {
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
    }
    
}

extension UIView{
    
    func dashBorder(border: CAShapeLayer, borderRadius: CGFloat = 0, borderWidth: CGFloat = 1, borderColor: UIColor = .white){
        border.strokeColor = UIColor.white.cgColor
        border.fillColor = nil
        border.lineDashPattern = [4, 4]
        border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.height * 0.4).cgPath
        border.frame = self.bounds
        self.layer.addSublayer(border)
    }
    
    func dropShadow(size:CGSize = CGSize(width: 1, height: 1), color:UIColor = .white, radius:CGFloat = 5, opacity:Float = 1){
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.clipsToBounds = false
    }
}
