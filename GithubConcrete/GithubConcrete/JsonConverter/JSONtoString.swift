//
//  JSONtoString.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

//Convrter do JSON para String
func convertToJSONString(from object: Any) -> String? {
    
    if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
        
        let objectString = String(data: objectData, encoding: .utf8)
        
        return objectString
    }
    
    return nil
}
