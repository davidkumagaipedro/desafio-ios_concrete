//
//  JSONtoDictionary.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit

//Converter do JSON para dicionario
func convertToDictionary(text: String) -> [String: Any]? {
    
    if let data = text.data(using: .utf8) {
        do {
            
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            
            print(error.localizedDescription)
            
        }
    }
    
    return nil
}
