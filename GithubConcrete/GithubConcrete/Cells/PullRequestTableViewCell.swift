//
//  PullRequestTableViewCell.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var tituloPullRequest: UILabel!
    @IBOutlet weak var descricaoPullRequest: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var name_owner: UILabel!
    @IBOutlet weak var image_owner: UIImageView!
    
    //setup de carga da cell
    func setup(pullrequest: PullRequest){
        self.image_owner.sd_setImage(with: URL(string: (pullrequest.userPullRequest?.image_user)!), placeholderImage: UIImage(named: "Unknown"), options: [.progressiveDownload, .continueInBackground], completed: nil)
        self.username.text = pullrequest.userPullRequest?.name_user
        self.name_owner.text = pullrequest.repository?.nomeRepositorio
        self.tituloPullRequest.text = pullrequest.infoPullRequest?.tituloPullRequest
        self.descricaoPullRequest.text = pullrequest.infoPullRequest?.bodyPullRequest
        
        self.image_owner.roundBorder()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
