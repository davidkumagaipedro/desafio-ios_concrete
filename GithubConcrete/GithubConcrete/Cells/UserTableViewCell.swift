//
//  UserTableViewCell.swift
//  GithubConcrete
//
//  Created by David Kumagai Pedro on 10/11/17.
//  Copyright © 2017 Senac BEPiD. All rights reserved.
//

import UIKit
import SDWebImage

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nomeRepositorio: UILabel!
    @IBOutlet weak var descricaoRepositorio: UILabel!
    
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var imagemAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    //setup de carga da cell
    func setup(user: Usuario){
        self.nomeRepositorio.text = user.usuarioGit?.repositorioNome
        self.descricaoRepositorio.text = user.usuarioGit?.description_repository
        self.forks.text = user.usuarioGit?.forks?.description
        self.stars.text = user.usuarioGit?.stars?.description
        self.username.text = user.ownerRepo?.name
        self.nome.text = user.usuarioGit?.full_name
        
        self.imagemAvatar.roundBorder()
        self.imagemAvatar.sd_setImage(with: URL(string: (user.ownerRepo?.avatar_url)!), placeholderImage: UIImage(named: "Unknown"), options: [.progressiveDownload, .continueInBackground], completed: nil)
        
        
    }

}
